function clone (obj) {
  if (obj.clone) return obj.clone()
  if (typeof obj === 'object') return JSON.parse(JSON.stringify(obj))
  return obj
}

export default function mergeDefaults (component, defaults) {
  for (let key in defaults) {
    if (component[key] === undefined) component[key] = clone(defaults[key])
  }
}
