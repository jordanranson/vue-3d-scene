import { Scene } from 'three'
import System from './System.js'

let _entity, _system, _component

const defaultOptions = {
  systems: {},

  tick () {
  },

  render () {
    if (this.camera) this.renderer.render(this.scene, this.camera.ref)
  }
}

export default class Program {
  get camera () {
    const entity = this._camera || (this._camera = this.findEntityByTag('camera'))
    if (!entity) return undefined

    const components = entity.components && entity.components.camera
    if (!components) return undefined

    return components.main
  }

  constructor (options) {
    this.scene = new Scene()
    this.renderer = undefined

    this.state = {}
    this.running = false
    this.pauseOnError = true

    this.onTick = (options.tick || defaultOptions.tick).bind(this)
    this.onRender = (options.render || defaultOptions.render).bind(this)

    this.systems = {}
    const systems = (options.systems || defaultOptions.systems)
    for (let systemName in systems) {
      this.systems[systemName] = new System(systems[systemName])
    }

    this.entities = []
    this.entityTable = {}

    this._curId = 1
    this._camera = undefined
    this._tickHandler = this.tick.bind(this)

    if (options.created) {
      options.created.call(this)
    }
  }

  nextId () {
    return (this._curId++).toString()
  }

  addEntity (components, tags = []) {
    const entity = {}

    entity.components = {}

    components.forEach((component) => {
      const id = component.id || this.nextId()
      const kind = component.kind

      delete component.kind
      entity.components[kind] = entity.components[kind] || {}
      entity.components[kind][id] = component

      if (!this.systems[kind]) return
      else if (!this.systems[kind].create) return
      this.systems[kind].create(entity, entity.components[kind][id])
    })

    entity.id = this.nextId()
    entity.tags = tags

    this.entities.push(entity)
    this.entityTable[entity.id] = entity

    return entity
  }

  destroyComponents (entity) {
    for (let system in entity.components) {
      if (!this.systems[system]) continue
      else if (!this.systems[system].destroy) continue
      for (let id in entity.components[system]) {
        this.systems[system].destroy(entity, entity.components[system][id], this)
      }
    }
  }

  removeEntity (entity) {
    const index = this.entities.indexOf(entity)
    if (index === -1) return false

    this.destroyComponents(entity)

    this.entities.splice(index, 1)
    delete this.entityTable[entity.id]

    return true
  }

  removeEntityById (id) {
    if (!this.entityTable[id]) return false

    this.destroyComponents(this.entityTable[id])

    const index = this.entities.findIndex((entity) => entity.id === id)

    this.entities.splice(index, 1)
    delete this.entityTable[id]

    return true
  }

  findEntityById (id) {
    return this.entityTable[id]
  }

  findEntityByTag (tag) {
    return this.entities.find((entity) => {
      return entity.tags.includes(tag)
    })
  }

  run () {
    if (this.running) return

    this.running = true
    this._tickHandler()
  }

  pause () {
    this.running = false
  }

  tick () {
    if (!this.running) return

    try {
      for (_entity of this.entities) {
        for (_system in this.systems) {
          if (!_entity.components[_system]) continue
          else if (!this.systems[_system]) continue
          else if (!this.systems[_system].update) continue
          for (_component in _entity.components[_system]) {
            this.systems[_system].update(_entity, _entity.components[_system][_component], this)
          }
        }
      }

      this.onTick(this)
      this.onRender(this)
    } catch (err) {
      if (this.pauseOnError) {
        this.pause()
        throw err
      } else {
        console.warn(err) // eslint-disable-line
      }
    }

    requestAnimationFrame(this._tickHandler)
  }
}
