class V3d {
  static toRad (deg) {
    return deg*Math.PI/180
  }
}

export default {
  install (Vue) {
    Vue.prototype.$v3d = V3d
  }
}
