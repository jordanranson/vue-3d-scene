export default require('./plugins/v3d.js').default

export const components = {
  V3dWebGlRenderer: require('./components/V3dWebGlRenderer.vue').default
}

export const mixins = {
  scene: require('./mixins/v3d-scene.js').default
}

export const systems = {
  perspectiveCamera: require('./systems/perspective-camera.js').default
}
