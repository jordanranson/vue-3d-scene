import { PerspectiveCamera, Vector3 } from 'three'
import mergeDefaults from '../merge-defaults.js'

const defaultComponent = {
  aspectRatio: 1920/1080,
  fov: 50,
  position: new Vector3(0, 0, 100),
  rotation: new Vector3(0, 0, 0),
  near: 0.1,
  far: 2000,
  orbitControls: false,
  zoom: 1
}

export default {
  create (entity, component) {
    mergeDefaults(component, defaultComponent)

    const { fov, aspectRatio, near, far } = component

    const camera = new PerspectiveCamera(fov, aspectRatio, near, far)
    camera.position.set(component.position)
    camera.rotation.set(component.rotation)
    camera.updateProjectionMatrix()

    component.ref = camera
  },

  update () {
  }
}
