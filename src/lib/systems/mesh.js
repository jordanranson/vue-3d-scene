import { Mesh } from 'three'
import mergeDefaults from '../merge-defaults.js'

const defaultComponent = {}

export default {
  create (entity, component, program) {
    mergeDefaults(component, defaultComponent)

    const geometry = component.geometry
    const material = component.material

    const mesh = new Mesh(geometry, material)
    program.scene.add(mesh)

    component.ref = mesh
  },

  destroy (entity, component, program) {
    program.scene.remove(component.ref)
  }
}
