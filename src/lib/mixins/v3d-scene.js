import Program from '../Program.js'

export default {
  program: undefined,

  provide () {
    return {
      v3dScene: this
    }
  },

  created () {
    this.createProgram()
  },

  methods: {
    createProgram () {
      if (!this.$options.program) {
        throw new Error('No program defined.')
      }

      this._program = new Program(this.$options.program)
    }
  }
}
