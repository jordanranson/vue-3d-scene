export default class System {
  constructor (options) {
    this.create = (options.create || (function () {})).bind(this)
    this.update = (options.update || (function () {})).bind(this)
  }
}
