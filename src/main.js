import Vue from 'vue'
import App from './Demo.vue'
import v3d from '@/lib/index.js'

Vue.use(v3d)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
