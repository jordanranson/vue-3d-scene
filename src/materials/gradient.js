import {
  ShaderMaterial,
  CustomBlending,
  Color
} from 'three'

import vertexShader from '@/shaders/gradient.vs'
import fragmentShader from '@/shaders/gradient.fs'

const BOTTOM_COLOR = new Color(222/255, 244/255, 241/255)
const TOP_COLOR    = new Color(255/255, 234/255, 219/255)

export default {
  create () {
    return new ShaderMaterial({
      vertexShader,
      fragmentShader,
      blending: CustomBlending,
      uniforms: {
        u_bottomColor: { value: BOTTOM_COLOR },
        u_topColor: { value: TOP_COLOR },
        u_bottomYPos: { value: -35 },
        u_topYPos: { value: 35 },
      }
    })
  }
}
