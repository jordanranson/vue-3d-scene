import { systems } from '@/lib/index.js'
import createDemoCamera from '@/demo-camera.js'
import createDemoMesh from '@/demo-mesh.js'

export default {
  systems: {
    camera: systems.perspectiveCamera
  },

  created () {
    this.addEntity(createDemoCamera(), [ 'camera' ])
    this.addEntity(createDemoMesh())
  }
}
