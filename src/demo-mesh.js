export default function createEntity () {
  const geometry = ''
  const material = ''

  return [
    {
      kind: 'mesh',
      geometry,
      material
    }
  ]
}
