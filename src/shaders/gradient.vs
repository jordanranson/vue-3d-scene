varying vec3 v_color;

uniform float u_bottomYPos;
uniform float u_topYPos;
uniform vec3 u_bottomColor;
uniform vec3 u_topColor;

void main()
{
    mat4 vp = projectionMatrix * viewMatrix;
    mat4 mvp = vp * modelMatrix;

    vec4 screenSpaceBottom = vp * vec4(0.0, u_bottomYPos, 0.0, 1.0);
    vec4 screenSpaceTop    = vp * vec4(0.0, u_topYPos, 0.0, 1.0);
    vec4 screenSpacePos    = mvp * vec4(position, 1.0);

    float t = (screenSpacePos.y - screenSpaceBottom.y) / (screenSpaceTop.y - screenSpaceBottom.y);
    v_color = mix(u_bottomColor, u_topColor, t);

    gl_Position = screenSpacePos;
}
